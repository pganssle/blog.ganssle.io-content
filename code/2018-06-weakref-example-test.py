import weakref
import gc

from threading import Lock
from weakref import WeakValueDictionary


class Example:
    """An example class"""
    def __init__(self, x):
        self.x = x


def get_example(x):
    # Either return the cached value or populate the cache
    return get_example.__cache.setdefault(x, Example(x))


def __clear_cache():
    get_example.__cache = WeakValueDictionary()


get_example.clear_cache = __clear_cache


# Create the initial empty cache
get_example.clear_cache()

###
# Tests

# Note: get_example_cache_lock is used here for thread safety
#       and test setup. See the code in the footnote below

CACHE_LOCK = Lock()


def get_example_cache_lock(f):
    # Provide a fresh cache and acquire the cache lock
    def test_inner(*args, **kwargs):
        with CACHE_LOCK:
            get_example.clear_cache()
            f(*args, **kwargs)
    return test_inner


@get_example_cache_lock
def test_get_example_cache():
    assert get_example('Key') is get_example('Key')


@get_example_cache_lock
def test_get_example_cache_weakref():
    key_example = get_example('Key')
    key_example_ref = weakref.ref(key_example)

    # Make sure this is a weak reference to the right thing
    assert get_example('Key') is key_example_ref()

    # Delete the only strong reference
    del key_example

    # Trigger garbage collection
    gc.collect()

    # key_example_ref() should be None at this point
    assert get_example('Key') is not key_example_ref()


@get_example_cache_lock
def test_get_example_cache_weakref_callback():
    key_example = get_example('Key')

    weakref_deleted = False

    def callback(obj):
        nonlocal weakref_deleted
        weakref_deleted = True

    key_example_ref = weakref.ref(key_example, callback)

    assert not weakref_deleted

    del key_example
    gc.collect()

    assert weakref_deleted

