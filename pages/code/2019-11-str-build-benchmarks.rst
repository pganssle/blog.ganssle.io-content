###############################
String building benchmarks code
###############################

:slug: 2019-11-str-build-benchmarks
:status: hidden
:author: Paul Ganssle
:summary: Code for benchmarking various ways to build up a string incrementally.

Below is the full code that can be used to run the three micro-benchmarks described in `my post about string concatenation <{filename}../../posts/2019-11-002-string-concat.rst>`_:

.. code-include:: ../../code/2019-11-str-build-benchmarks.py
    :lexer: py3


You can `download the code here <{filename}/code/2019-11-str-build-benchmarks.py>`_.
